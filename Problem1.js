function findCarById(data, id) {
  if (Array.isArray(data) && typeof id === "number" && data.length !== 0) {
    for (let index = 0; index < data.length; index++) {
      if (data[index].id == id) {
        return data[index];
      }
    }
  }
  return [];
}

module.exports = { findCarById };
