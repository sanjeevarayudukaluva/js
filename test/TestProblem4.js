// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const inventory = require("../CarData.js");
const allCarYearsData = require("../Problem4.js");

try {
  const carYears = allCarYearsData.yearOfAllCars(inventory.inventory);
  if (carYears === null || carYears === undefined || carYears.length === 0) {
    throw new Error("carYear is null, undefined, or empty");
  }
  console.log(`${carYears}`);
} catch (error) {
  console.log(error);
}
