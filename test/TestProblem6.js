// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console
const inventory = require("../CarData.js");
const onlyBmwAudiCarsData = require("../Problem6.js");

try {
  const audiBmwCar = onlyBmwAudiCarsData.bmwAudiCars(inventory.inventory);
  if (
    audiBmwCar === null ||
    audiBmwCar.length === 0 ||
    audiBmwCar === undefined
  ) {
    throw new Error("data not found");
  }
  console.log(`${JSON.stringify(audiBmwCar)}`);
} catch (error) {
  console.log(error);
}
