// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const inventory = require("../CarData.js");
const sortedCarModels = require("../Problem3.js");

try {
  const carModels = sortedCarModels.sortCarModelsAlphabetically(
    inventory.inventory
  );
  if (carModels === null || carModels === undefined || carModels.length === 0) {
    throw new Error("Data  is null, undefined, or empty");
  }
  console.log(`${carModels}`);
} catch (error) {
  console.log(error);
}
