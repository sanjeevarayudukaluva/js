// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
const inventory = require("../CarData.js");
const allCarYearsData = require("../Problem4.js");

const carsYearsBefore_2000 = [];
try {
  const carYears = allCarYearsData.yearOfAllCars(inventory.inventory);

  if (carYears === null || carYears === undefined || carYears.length === 0) {
    throw new Error("Data  is null, undefined, or empty");
  }
  for (let index = 0; index < carYears.length; index++) {
    if (carYears[index] < 2000) {
      carsYearsBefore_2000.push(carYears[index]);
    }
  }
  console.log(
    `${carsYearsBefore_2000} and length of cars before 2000 is ${carsYearsBefore_2000.length}`
  );
} catch (error) {
  console.log(error);
}