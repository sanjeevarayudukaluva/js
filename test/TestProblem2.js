// The dealer needs the information on the last car in their inventory. Execute a function to find what the make
const inventory = require("../CarData.js");
const findLatsCar = require("../Problem2.js");

try {
  const lastCar = findLatsCar.findLatsCar(inventory.inventory);
  if (lastCar === null || lastCar === undefined || lastCar.length === 0) {
    throw new Error("Data  is null, undefined, or empty");
  }
  console.log(`Last car is a ${lastCar.car_make}  ${lastCar.car_model}`);
} catch (error) {
  console.log(error);
}
