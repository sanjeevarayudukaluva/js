//The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car

const findCar = module.require("../Problem1.js");
const inventory =module. require("../CarData.js");

try {
  const car_33 = findCar.findCarById(inventory.inventory, 33);

  if (car_33 == null || car_33 == undefined || car_33.length === 0) {
    throw new Error("Data for car 33 is null, undefined, or empty");
  }

  console.log(
    `Car 33 is a ${car_33.car_year} ${car_33.car_make} ${car_33.car_model}`
  );
} catch (error) {
  console.log(error.message);
}
