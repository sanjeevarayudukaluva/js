function yearOfAllCars(data) {
    const carYears = [];
    if (Array.isArray(data) && data.length !== 0) {
      for (let index = 0; index < data.length; index++) {
        carYears.push(data[index].car_year);
      }
    }
    return carYears;
  }
  module.exports = { yearOfAllCars };
