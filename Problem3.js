function sortCarModelsAlphabetically(data) {
    const carModels = [];
    if (Array.isArray(data) && data.length !== 0) {
      for (let index = 0; index < data.length; index++) {
        carModels.push(data[index].car_model);
      }
      carModels.sort();
    }
    return carModels;
  }
  module.exports = { sortCarModelsAlphabetically };
