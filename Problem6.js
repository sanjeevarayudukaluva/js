function bmwAudiCars(data) {
    const audiBmwCarsData = [];
    if (Array.isArray(data) && data.length !== 0) {
      for (let index = 0; index < data.length; index++) {
        if (data[index].car_make === "BMW" || data[index].car_make === "Audi") {
          audiBmwCarsData.push(data[index]);
        }
      }
    }
    return audiBmwCarsData;
  }
  module.exports={bmwAudiCars};
